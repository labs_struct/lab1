import random
import time
import numpy as np


def timerfunc(func):
    """
    Декоратор для определения времени работы функции
    """

    def function_timer(*args, **kwargs):
        """
        врапер
        """
        start = time.time()
        value = func(*args, **kwargs)
        end = time.time()
        runtime = end - start
        msg = "Время работы функции {func} : {time} секунд"
        print(msg.format(func=func.__name__,
                         time=runtime))
        return value

    return function_timer


@timerfunc
def generate_matrix():
    res = []
    for line in range(0, 6400):
        row_arr = []
        for row in range(0, 6400):
            row_arr.append(random.random())
        res.append(row_arr)
    return np.array(res)


@timerfunc
def multiply(A, B):
    return A * B


@timerfunc
def start():
    A = generate_matrix()
    B = generate_matrix()
    multiply(A, B)

if __name__ == '__main__':
    start()

